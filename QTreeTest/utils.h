#pragma once
#include <tuple>
#include <d2d1.h>
#include "RbPolyLine.h"
#include "HVector2d.h"


#define PI 3.1415926535898


template<class Interface>
inline void
SafeRelease(
	Interface** ppInterfaceToRelease
)
{
	if (*ppInterfaceToRelease != nullptr)
	{
		(*ppInterfaceToRelease)->Release();

		(*ppInterfaceToRelease) = nullptr;
	}
}


bool isPointOnSegment(HPoint2d const& p, RbLineSeg const& seg);


// 角度转弧度
double Degree2Radius(double s);

// 弧度转角度 
double Radius2Degree(double s);

// 点沿单位向量移动的距离
HPoint2d PointExtendDis(HPoint2d pt, HPoint2d vec, double dis);

// 一点绕另外一点逆时针旋转
HPoint2d RotateByPoint(HPoint2d CenterPt, HPoint2d SrcPt, double Dgree);

// 线段垂直向上单位向量
HPoint2d LineSetVectorTop(RbLineSeg Line);


// 点乘
double DotProduct(HPoint2d vec1, HPoint2d vec2);

// 叉乘
HPoint2d CrossProduct(HPoint2d vec1, HPoint2d vec2);

