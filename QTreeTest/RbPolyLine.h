#pragma once
#include <vector>
#include <d2d1.h>
#include <tuple>
#include <deque>
#include <memory>
#include <d2d1helper.h>
#include "HVector2d.h"


#define LINESEG_DX(p1, p2) \
	p2.x - p1.x;

#define LINGSEG_DY(p1, p2) \
	p2.y - p1.y;


struct IBox
{
	virtual ~IBox() {}
	virtual D2D1_RECT_F GetBox() { return D2D1_RECT_F(0, 0, 0, 0); };
	virtual void DrawBox(ID2D1HwndRenderTarget* pRenderTarget = nullptr) { pRenderTarget; }

protected:
	D2D1_RECT_F m_Box;
};

struct IGetLength
{
	enum class enumOff : int
	{
		enumSet,
		enumEnd,
	};

	
	virtual ~IGetLength() {}
	virtual double GetLength() = 0;
	virtual double GetParam(HPoint2d const& pt, enumOff off = enumOff::enumSet) = 0;
};

class IDrawShape
{
public:
	virtual ~IDrawShape() {}
	virtual void DrawShape(ID2D1HwndRenderTarget* pRenderTarget) = 0;
};

class IGetNearestPoint
{
public:
	~IGetNearestPoint() {}

	virtual std::tuple<HPoint2d, double> GetNearestPoint(HPoint2d const& pt) = 0;
};


class CHroadCurve
	: public IDrawShape
	, public IBox
	, public IGetNearestPoint
{
public:
	CHroadCurve();
	CHroadCurve(HPoint2d p1, HPoint2d p2);
	virtual ~CHroadCurve();

	HPoint2d StartPt()const { return m_StartP; }
	HPoint2d EndPt() const { return m_EndP; }

	std::tuple<HPoint2d, double> GetNearestPoint(HPoint2d const& pt) override;

protected:
	HPoint2d m_StartP;
	HPoint2d m_EndP;
};

class RbLineSeg 
	: public CHroadCurve
	, public IGetLength
{
public:
	RbLineSeg(HPoint2d const& p1, HPoint2d const& p2);

	HPoint2d const& StartPt()const { return m_StartP; }
	HPoint2d const& EndPt() const { return m_EndP; }

	virtual ~RbLineSeg() {}

	virtual double GetParam(HPoint2d const& pt, enumOff off = enumOff::enumSet) override;
	virtual double GetLength() override;

	virtual void DrawShape(ID2D1HwndRenderTarget* pRenderTarget) override;

	virtual D2D1_RECT_F GetBox() override;

	virtual void DrawBox(ID2D1HwndRenderTarget* pRenderTarget = nullptr) override;


	std::tuple<HPoint2d, double> GetNearestPoint(HPoint2d const& pt) override;

private:
};

class RbCircleLine
	: public CHroadCurve
	, public IGetLength
{
public:
	RbCircleLine();
	RbCircleLine(HPoint2d const& start, HPoint2d const& end);
	virtual ~RbCircleLine();

	virtual void DrawBox(ID2D1HwndRenderTarget* pRenderTarget) override;
	D2D1_RECT_F GetBox() override;

	void SetParam(HPoint2d start, HPoint2d end, D2D1_SIZE_F size, bool IsClockWise, bool arcSize);

	double GetLength() override;
	double GetParam(HPoint2d const& pt, enumOff off = enumOff::enumSet) override;
	void DrawShape(ID2D1HwndRenderTarget* pRenderTarget) override;

	std::tuple<HPoint2d, double> GetNearestPoint(HPoint2d const& p) override;

private:
	D2D1_SIZE_F m_Size;
	FLOAT m_RotationAngle = 0;
	D2D1_SWEEP_DIRECTION m_SweepDirection;
	D2D1_ARC_SIZE m_ArcSize;
	ID2D1PathGeometry* m_PathGeometry = nullptr;
};


class RbPolyLine 
	: public CHroadCurve
	, public IGetLength
{
public:
	RbPolyLine();
	virtual ~RbPolyLine() {}

	void AddLineSegBack(HPoint2d const& pt);
	void AddLineSegFront(HPoint2d const& pt);
	void AddLineSegBack(HPoint2d const& pt1, HPoint2d const& pt2);
	void AddLineSegFront(HPoint2d const& pt1, HPoint2d const& pt2);
	void AddArcBack(HPoint2d const& pt, D2D1_SIZE_F size, bool IsClockWise, bool arcSize);

	int LineSegCount() const;
	CHroadCurve* GetCurve(int index);

	virtual void DrawBox(ID2D1HwndRenderTarget* pRenderTarget) override;
	virtual D2D1_RECT_F GetBox();

	virtual double GetParam(HPoint2d const& p, enumOff off = enumOff::enumSet) override;
	virtual double GetLength() override;
	virtual void DrawShape(ID2D1HwndRenderTarget* pRenderTarget) override;

protected:
	std::deque<std::shared_ptr<CHroadCurve>> m_PolyPoint;
};

