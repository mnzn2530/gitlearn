#pragma once
#include <d2d1.h>


class HPoint2d
{
public:
	HPoint2d();
	HPoint2d(float px, float py);

	HPoint2d UnitVector();

	HPoint2d operator+(HPoint2d const& pt);
	HPoint2d operator-(HPoint2d const& pt) const;
	HPoint2d operator*(double s);
	HPoint2d operator/(double d);

	operator D2D1_POINT_2F() const;

	double ClockAngelByOrg();
	float X() const;
	float Y() const;

	float GetMod();
	float GetMod2();

	float& X();
	float& Y();

protected:
	float m_x;
	float m_y;
};

typedef HPoint2d HVector2d;

