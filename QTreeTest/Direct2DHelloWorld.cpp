// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved


#include "Direct2DHelloWorld.h"
#include "trace.h"
#include <string>
#include "HroadD2D1Factory.h"
#include "utils.h"
#include "HroadBrushManager.h"


//
// Provides the entry point to the application.
//
int WINAPI WinMain(
    HINSTANCE /*hInstance*/,
    HINSTANCE /*hPrevInstance*/,
    LPSTR /*lpCmdLine*/,
    int /*nCmdShow*/
    )
{
    // Ignore the return value because we want to run the program even in the
    // unlikely event that HeapSetInformation fails.
    HeapSetInformation(nullptr, HeapEnableTerminationOnCorruption, nullptr, 0);

    if (SUCCEEDED(CoInitialize(nullptr)))
    {
        {
            DemoApp app;

            if (SUCCEEDED(app.Initialize()))
            {
                app.RunMessageLoop();
            }
        }

        CoUninitialize();
    }

    return 0;
}

//
// Initialize members.
//
DemoApp::DemoApp() :
    m_hwnd(nullptr),
    m_pDWriteFactory(nullptr),
    m_pRenderTarget(nullptr),
    m_pTextFormat(nullptr)
{

}

//
// Release resources.
//
DemoApp::~DemoApp()
{
    SafeRelease(&m_pDWriteFactory);
    SafeRelease(&m_pRenderTarget);
    SafeRelease(&m_pTextFormat);
}

//
// Creates the application window and initializes
// device-independent resources.
//
HRESULT DemoApp::Initialize()
{
    HRESULT hr;

    // Initialize device-indpendent resources, such
    // as the Direct2D factory.
    hr = CreateDeviceIndependentResources();
    if (SUCCEEDED(hr))
    {
        // Register the window class.
        WNDCLASSEX wcex = { sizeof(WNDCLASSEX) };
        wcex.style         = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc   = DemoApp::WndProc;
        wcex.cbClsExtra    = 0;
        wcex.cbWndExtra    = sizeof(LONG_PTR);
        wcex.hInstance     = HINST_THISCOMPONENT;
        wcex.hbrBackground = nullptr;
        wcex.lpszMenuName  = nullptr;
        wcex.hCursor       = LoadCursor(nullptr, IDC_ARROW);
        wcex.lpszClassName = L"D2DDemoApp";

        RegisterClassEx(&wcex);

        // Create the application window.
        //
        // Because the CreateWindow function takes its size in pixels, we
        // obtain the system DPI and use it to scale the window size.
        UINT nDpi = GetDpiForSystem();

        m_hwnd = CreateWindow(
            L"D2DDemoApp",
            L"Direct2D Demo Application",
            WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            static_cast<UINT>(ceil(1200 * (nDpi & 0XFFFF) / 96.f)),
            static_cast<UINT>(ceil(600.f * (nDpi & 0xffff0000 >> 16) / 96.f)),
            nullptr,
            nullptr,
            HINST_THISCOMPONENT,
            this
            );
        hr = m_hwnd ? S_OK : E_FAIL;
        if (SUCCEEDED(hr))
        {
            ShowWindow(m_hwnd, SW_SHOWNORMAL);

            UpdateWindow(m_hwnd);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    ReadLineFile();


    return hr;
}


//
// Create resources which are not bound
// to any device. Their lifetime effectively extends for the
// duration of the app. These resources include the Direct2D and
// DirectWrite factories,  and a DirectWrite Text Format object
// (used for identifying particular font characteristics).
//
HRESULT DemoApp::CreateDeviceIndependentResources()
{
    static const WCHAR msc_fontName[] = L"Verdana";
    static const FLOAT msc_fontSize = 10;
    HRESULT hr = E_FAIL;

    // Create a Direct2D factory.
    ID2D1Factory* pFactory = HroadD2D1Factory::GetInstance().GetFactory();
    if (pFactory != nullptr)
    {
        // Create a DirectWrite factory.
        hr = DWriteCreateFactory(
            DWRITE_FACTORY_TYPE_SHARED,
            __uuidof(m_pDWriteFactory),
            reinterpret_cast<IUnknown **>(&m_pDWriteFactory)
            );
    }

    if (SUCCEEDED(hr))
    {
        // Create a DirectWrite text format object.
        hr = m_pDWriteFactory->CreateTextFormat(
            msc_fontName,
            nullptr,
            DWRITE_FONT_WEIGHT_NORMAL,
            DWRITE_FONT_STYLE_NORMAL,
            DWRITE_FONT_STRETCH_NORMAL,
            msc_fontSize,
            L"", //locale
            &m_pTextFormat
            );
    }

    if (SUCCEEDED(hr))
    {
        // Center the text horizontally and vertically.
        m_pTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
        m_pTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);

    }

    return hr;
}

//
//  This method creates resources which are bound to a particular
//  Direct3D device. It's all centralized here, in case the resources
//  need to be recreated in case of Direct3D device loss (eg. display
//  change, remoting, removal of video card, etc).
//
HRESULT DemoApp::CreateDeviceResources()
{
    HRESULT hr = S_OK;

    if (!m_pRenderTarget)
    {

        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(
            rc.right - rc.left,
            rc.bottom - rc.top
            );

        ID2D1Factory* pD2dFactory = HroadD2D1Factory::GetInstance().GetFactory();
        if (pD2dFactory == nullptr)
        {
            return E_FAIL;
        }

        // Create a Direct2D render target.
        hr = pD2dFactory->CreateHwndRenderTarget(
            D2D1::RenderTargetProperties(),
            D2D1::HwndRenderTargetProperties(m_hwnd, size),
            &m_pRenderTarget
            );
        if (SUCCEEDED(hr))
        {
            m_pRenderTarget->SetAntialiasMode(D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
            HroadBrushManager::GetInstance().Init(m_pRenderTarget);
        }
    }

    return hr;
}


//
//  Discard device-specific resources which need to be recreated
//  when a Direct3D device is lost
//
void DemoApp::DiscardDeviceResources()
{
    SafeRelease(&m_pRenderTarget);
}

//
// The main window message loop.
//
void DemoApp::RunMessageLoop()
{
    MSG msg;

    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

int DemoApp::ReadLineFile()
{
	m_Line1.AddLineSegBack({100, 100 }, { 100, 200 });
	m_Line1.AddLineSegBack({200, 200 });
	m_Line1.AddLineSegBack({300, 300 });
	m_Line1.AddLineSegBack({500, 400 });
    m_Line1.AddArcBack({ 600, 400 }, { 200, 200 }, true, false);
	m_Line1.AddLineSegBack({600, 300 });
    m_Line1.AddArcBack({ 700, 200 }, { 200, 200 }, true, false);
    m_Line1.AddArcBack({ 800, 200 }, { 200, 200 }, true, false);
	m_Line1.AddLineSegBack({967, 400 });
    m_Line1.AddArcBack({ 999, 600 }, { 200, 200 }, true, false);

    return 0;
}

//
//  Called whenever the application needs to display the client
//  window. This method writes "Hello, World"
//
//  Note that this function will not render anything if the window
//  is occluded (e.g. when the screen is locked).
//  Also, this function will automatically discard device-specific
//  resources if the Direct3D device disappears during function
//  invocation, and will recreate the resources the next time it's
//  invoked.
//
HRESULT DemoApp::OnRender()
{
    HRESULT hr;


    hr = CreateDeviceResources();

    if (SUCCEEDED(hr) && !(m_pRenderTarget->CheckWindowState() & D2D1_WINDOW_STATE_OCCLUDED))
    {
        // Retrieve the size of the render target.
        D2D1_SIZE_F renderTargetSize = m_pRenderTarget->GetSize();
        m_pRenderTarget->BeginDraw();
        m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
        m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::Black));

        m_Line1.DrawShape(m_pRenderTarget);
        m_Line1.DrawBox(m_pRenderTarget);

		int nCount = m_Line1.LineSegCount();
		for (int i = 0; i < nCount; ++i)
		{
			CHroadCurve* lineSeg = m_Line1.GetCurve(i);
            lineSeg->DrawBox(m_pRenderTarget);
		}

        DrawDemision(m_pRenderTarget, m_Line1);
        DrawMousePos(m_pRenderTarget);

        hr = m_pRenderTarget->EndDraw();
        if (hr == D2DERR_RECREATE_TARGET)
        {
            hr = S_OK;
            DiscardDeviceResources();
        }
    }

    return hr;
}

//
//  If the application receives a WM_SIZE message, this method
//  resizes the render target appropriately.
//
void DemoApp::OnResize(UINT width, UINT height)
{
    if (m_pRenderTarget)
    {
        D2D1_SIZE_U size;
        size.width = width;
        size.height = height;

        // Note: This method can fail, but it's okay to ignore the
        // error here -- it will be repeated on the next call to
        // EndDraw.
        m_pRenderTarget->Resize(size);
    }
}


std::tuple<D2D1_POINT_2F, double> DemoApp::GetNearestPoint(RbPolyLine& PolyLine, HPoint2d const& pt)
{
	double MinDis = 0xffffffff;
	D2D1_POINT_2F MinPt = { 0 };


	for (int i = 0; i < PolyLine.LineSegCount(); ++i)
	{
		CHroadCurve* lineSeg = PolyLine.GetCurve(i);
		auto [p, dis] = lineSeg->GetNearestPoint(pt);

		m_pRenderTarget->DrawEllipse(D2D1_ELLIPSE((D2D1_POINT_2F)p, 2, 2), HroadBrushManager::GetInstance().GetGreenBrush());
		if (dis < MinDis)
		{
			MinDis = dis;
			MinPt = p;
		}
	}

    return std::make_tuple(MinPt, MinDis);
}

//
// The window message handler.
//
LRESULT CALLBACK DemoApp::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    if (message == WM_CREATE)
    {
        LPCREATESTRUCT pcs = (LPCREATESTRUCT)lParam;
        DemoApp *pDemoApp = (DemoApp *)pcs->lpCreateParams;

        ::SetWindowLongPtrW(
            hwnd,
            GWLP_USERDATA,
            reinterpret_cast<LONG_PTR>(pDemoApp)
            );

        result = 1;
    }
    else
    {
        DemoApp *pDemoApp = reinterpret_cast<DemoApp *>(
            ::GetWindowLongPtrW(
                hwnd,
                GWLP_USERDATA
                ));

        bool wasHandled = false;

        if (pDemoApp)
        {
            switch (message)
            {
            case WM_SIZE:
                {
                    UINT width = LOWORD(lParam);
                    UINT height = HIWORD(lParam);
                    pDemoApp->OnResize(width, height);
                }
                wasHandled = true;
                result = 0;
                break;

            case WM_PAINT:
            case WM_DISPLAYCHANGE:
                {
                    PAINTSTRUCT ps;
                    BeginPaint(hwnd, &ps);
                    pDemoApp->OnRender();
                    EndPaint(hwnd, &ps);
                }

                wasHandled = true;
                result = 0;
                break;

            case WM_DESTROY:
                {
                    PostQuitMessage(0);
                }

                wasHandled = true;
                result = 1;
                break;

			case WM_MOUSEMOVE:
			{
                UINT nPos = (UINT)lParam;
                pDemoApp->m_CurrPos.X() = LOWORD(nPos);
                pDemoApp->m_CurrPos.Y() = HIWORD(nPos);
                ::InvalidateRect(hwnd, nullptr, true);
			}

			break;
            }
           
        }

        if (!wasHandled)
        {
            result = DefWindowProc(hwnd, message, wParam, lParam);
        }
    }

    return result;
}

void DemoApp::DrawMousePos(ID2D1HwndRenderTarget* pRenderTarget)
{
    wchar_t buf[4096] = L"";
    wsprintf(buf, L"x = %d, y = %d", (int)m_CurrPos.X(), (int)m_CurrPos.Y());
    pRenderTarget->DrawText(buf, (UINT32)wcslen(buf), m_pTextFormat, D2D1_RECT_F(0, 0, 200, 10), 
        HroadBrushManager::GetInstance().GetWhiteBrush());
}

void DemoApp::DrawDemision(ID2D1HwndRenderTarget* pRenderTarget, RbPolyLine& PolyLine)
{
    auto const& [MinPt, MinDis] = GetNearestPoint(PolyLine, m_CurrPos);


	HDEBUG(L"%lf,%lf\n", MinPt.x, MinPt.y);
	pRenderTarget->DrawLine(m_CurrPos, MinPt, HroadBrushManager::GetInstance().GetWhiteBrush());
}
