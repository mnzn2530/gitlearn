#include "HroadBrushManager.h"
#include "trace.h"
#include "utils.h"


HroadBrushManager::HroadBrushManager()
{

}

HroadBrushManager::~HroadBrushManager()
{
	SafeRelease(&m_pBrushes[0]);
	SafeRelease(&m_pBrushes[1]);
	SafeRelease(&m_pBrushes[2]);
}

void HroadBrushManager::Init(ID2D1HwndRenderTarget* pRenderTarget)
{
	HRESULT hr = E_FAIL;
	ID2D1SolidColorBrush* pBrush = nullptr;


	// ������ɫ��ˢ
	hr = pRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::White),
		&pBrush);
	m_pBrushes.push_back(pBrush);

	hr = pRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::Green),
		&pBrush);
	m_pBrushes.push_back(pBrush);

	hr = pRenderTarget->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF::Red),
		&pBrush);
	m_pBrushes.push_back(pBrush);
}

HroadBrushManager& HroadBrushManager::GetInstance()
{
	static HroadBrushManager instance;
	return instance;
}

ID2D1SolidColorBrush* HroadBrushManager::GetWhiteBrush()
{
	return m_pBrushes[0];
}

ID2D1SolidColorBrush* HroadBrushManager::GetGreenBrush()
{
	return m_pBrushes[1];
}

ID2D1SolidColorBrush* HroadBrushManager::GetRedBrush()
{
	return m_pBrushes[2];
}

ID2D1SolidColorBrush* HroadBrushManager::GetBrush(int index)
{
	if (index < 0 || index >= m_pBrushes.size())
	{
		return nullptr;
	}

	return m_pBrushes[index];
}

