#include "HVector2d.h"
#include <corecrt_math.h>
#include "utils.h"


HPoint2d::HPoint2d()
{
	m_x = 0;
	m_y = 0;
}

HPoint2d::HPoint2d(float px, float py)
{
	m_x = px;
	m_y = py;
}

HPoint2d HPoint2d::UnitVector()
{
	double len = sqrt(m_x * m_x + m_y * m_y);
	if (len == 0)
	{
		HPoint2d out = *this;
		return out;
	}

	HPoint2d ptOut(float(m_x / len), float(m_y / len));
	return ptOut;
}

HPoint2d HPoint2d::operator+(HPoint2d const& pt)
{
	HPoint2d ptout;
	ptout.m_x = this->m_x + pt.m_x;
	ptout.m_y = this->m_y + pt.m_y;
	return ptout;
}

HPoint2d HPoint2d::operator-(HPoint2d const& pt) const
{
	HPoint2d ptout;
	ptout.m_x = this->m_x - pt.m_x;
	ptout.m_y = this->m_y - pt.m_y;
	return ptout;
}

HPoint2d HPoint2d::operator*(double s)
{
	HPoint2d ptout;
	ptout.m_x = float(this->m_x * s);
	ptout.m_y = float(this->m_y * s);
	return ptout;
}

HPoint2d HPoint2d::operator/(double d)
{
	HPoint2d ptout;
	ptout.m_x = float(this->m_x / d);
	ptout.m_y = float(this->m_y / d);
	return ptout;
}

double HPoint2d::ClockAngelByOrg()
{
	// arc tan 求出来的是弧度
	double angel = atan2(m_y, m_x);
	if (angel < 0)
	{
		angel += 2 * PI;
	}

	angel = Radius2Degree(angel);
	return angel;
}

float HPoint2d::X() const
{
	return m_x;
}

float HPoint2d::Y() const
{
	return m_y;
}

float HPoint2d::GetMod()
{
	return sqrt(X() * X() + Y() * Y());
}

float HPoint2d::GetMod2()
{
	return X() * X() + Y() * Y();
}

float& HPoint2d::X()
{
	return m_x;
}

float& HPoint2d::Y()
{
	return m_y;
}

HPoint2d::operator D2D1_POINT_2F() const
{
	return D2D1_POINT_2F(X(), Y());
}