#pragma once


#define HDEBUG(fmt, ...) \
{\
	wchar_t buf[4096] = L"";\
	swprintf_s(buf, 4095, fmt, __VA_ARGS__);\
	OutputDebugString(buf);\
}