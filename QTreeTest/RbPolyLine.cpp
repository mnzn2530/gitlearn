#include "RbPolyLine.h"
#include <optional>
#include <stdexcept>
#include "HroadD2D1Factory.h"
#include "utils.h"
#include "HroadBrushManager.h"
#include <d2d1helper.h>


RbPolyLine::RbPolyLine()
{

}

void RbPolyLine::AddLineSegBack(HPoint2d const& pt)
{
	if (m_PolyPoint.size() < 1)
	{
		return;
	}

	m_PolyPoint.push_back(std::make_shared<RbLineSeg>(m_PolyPoint.back()->EndPt(), pt));
}

void RbPolyLine::AddLineSegBack(HPoint2d const& pt1, HPoint2d const& pt2)
{
	m_PolyPoint.push_back(std::make_shared<RbLineSeg>(pt1, pt2));
}

void RbPolyLine::AddLineSegFront(HPoint2d const& pt)
{
	if (m_PolyPoint.size() < 1)
	{
		return;
	}

	m_PolyPoint.push_front(std::make_shared<RbLineSeg>(m_PolyPoint.front()->EndPt(), pt));
}

void RbPolyLine::AddLineSegFront(HPoint2d const& pt1, HPoint2d const& pt2)
{
	m_PolyPoint.push_front(std::make_shared<RbLineSeg>(pt1, pt2));
}

void RbPolyLine::AddArcBack(HPoint2d const& pt, D2D1_SIZE_F size, bool IsClockWise, bool arcSize)
{
	if (m_PolyPoint.size() < 1)
	{
		return;
	}

	std::shared_ptr<RbCircleLine> pNewArc = std::make_shared<RbCircleLine>();
	pNewArc->SetParam(m_PolyPoint.back()->EndPt(), pt, size, IsClockWise, arcSize);
	m_PolyPoint.push_back(pNewArc);
}

int RbPolyLine::LineSegCount() const
{
	if (m_PolyPoint.size() == 0)
	{
		return 0;
	}

	return (int)m_PolyPoint.size();
}

CHroadCurve* RbPolyLine::GetCurve(int index)
{
	if (index < 0 || index > m_PolyPoint.size() - 1)
	{
		return nullptr;
	}

	return m_PolyPoint[index].get();
}

void RbPolyLine::DrawBox(ID2D1HwndRenderTarget* pRenderTarget /*= nullptr*/)
{
	const D2D1_RECT_F& myBox = GetBox();
	pRenderTarget->DrawRectangle(myBox, HroadBrushManager::GetInstance().GetWhiteBrush(), 0.5);
}

D2D1_RECT_F RbPolyLine::GetBox()
{
	if (m_PolyPoint.size() == 0)
	{
		return m_Box;
	}

	D2D1_RECT_F MinBox = m_PolyPoint[0]->GetBox();
	m_Box.left = MinBox.left;
	m_Box.right = MinBox.right;
	m_Box.top = MinBox.top;
	m_Box.bottom = MinBox.bottom;

	for (auto& item : m_PolyPoint)
	{
		D2D1_RECT_F rc = item->GetBox();
		if (rc.left < m_Box.left)
		{
			m_Box.left = rc.left;
		}

		if (rc.right > m_Box.right)
		{
			m_Box.right = rc.right;
		}

		if (rc.top < m_Box.top)
		{
			m_Box.top = rc.top;
		}

		if (rc.bottom > m_Box.bottom)
		{
			m_Box.bottom = rc.bottom;
		}
	}

	return m_Box;
}

double RbPolyLine::GetParam(HPoint2d const& p, IGetLength::enumOff off /*= enumSet*/)
{
	return 0;
}

double RbPolyLine::GetLength()
{
	return 0;
}

void RbPolyLine::DrawShape(ID2D1HwndRenderTarget* pRenderTarget)
{
	int nCount = LineSegCount();
	for (int i = 0; i < nCount; ++i)
	{
		CHroadCurve* lineSeg = GetCurve(i);
		lineSeg->DrawShape(pRenderTarget);
	}
}

RbLineSeg::RbLineSeg(HPoint2d const& p1, HPoint2d const& p2)
	: CHroadCurve(p1, p2)
{
	if (StartPt().X() > EndPt().X())
	{
		m_Box.left = EndPt().X();
		m_Box.right = StartPt().X();
	}
	else
	{
		m_Box.left = StartPt().X();
		m_Box.right = EndPt().X();
	}

	if (StartPt().Y() > EndPt().Y())
	{
		m_Box.top = EndPt().Y();
		m_Box.bottom = StartPt().Y();
	}
	else
	{
		m_Box.top = StartPt().Y();
		m_Box.bottom = EndPt().Y();
	}
}

double RbLineSeg::GetParam(HPoint2d const& pt, enumOff off)
{
	HPoint2d p = pt;
	bool IsOnSeg = isPointOnSegment(pt, *this);
	if (!IsOnSeg)
	{
		auto [p1, dis] = GetNearestPoint(pt);
		p = p1;
	}

	if (off == enumOff::enumSet)
	{
		double xLen = pow(p.X() - m_StartP.X(), 2);
		double yLen = pow(p.Y() - m_StartP.Y(), 2);
		return sqrt(xLen + yLen);
	}
	else
	{
		double xLen = pow(m_EndP.X() - p.X(), 2);
		double yLen = pow(m_EndP.Y() - p.Y(), 2);
		return sqrt(xLen + yLen);
	}
}

double RbLineSeg::GetLength()
{
	return GetParam(m_EndP);
}

void RbLineSeg::DrawShape(ID2D1HwndRenderTarget* pRenderTarget)
{
	pRenderTarget->DrawLine(StartPt(), EndPt(), HroadBrushManager::GetInstance().GetGreenBrush());
}

D2D1_RECT_F RbLineSeg::GetBox()
{
	return m_Box;
}

void RbLineSeg::DrawBox(ID2D1HwndRenderTarget* pRenderTarget /*= nullptr*/)
{
	const D2D1_RECT_F& myBox = GetBox();
	pRenderTarget->DrawRectangle(myBox, HroadBrushManager::GetInstance().GetWhiteBrush(), 0.5);
}

std::tuple<HPoint2d, double> RbLineSeg::GetNearestPoint(HPoint2d const& pt0)
{
	HPoint2d pt1 = StartPt();
	HPoint2d pt2 = EndPt();
	
	HVector2d vec12 = pt2 - pt1;
	HVector2d vec10 = pt0 - pt1;
	HVector2d vec20 = pt0 - pt2;
	float cross = DotProduct(vec12, vec10);

	// 大于90度
	if (cross <= 0)
	{
		return std::make_tuple(pt1, vec10.GetMod());
	}

	// vec12模的平方
	float Pow2Vec12Mod = vec12.GetMod2();
	if (cross >= Pow2Vec12Mod)
	{
		return std::make_tuple(pt2, vec20.GetMod());
	}

	// 这里求的是投影
	// https://blog.csdn.net/lz0499/article/details/90311496
	float r = cross / Pow2Vec12Mod;
	HVector2d ProjectiveVec = vec12 * r;
	HPoint2d DstPt = ProjectiveVec + pt1;
	HPoint2d DstVec = DstPt - pt0;
	return  std::make_tuple(DstPt, DstVec.GetMod());
}

RbCircleLine::RbCircleLine(HPoint2d const& start, HPoint2d const& end)
	: CHroadCurve(start, end)
{
	SetParam(start, end, m_Size, false, true);
}

RbCircleLine::RbCircleLine()
{

}

RbCircleLine::~RbCircleLine()
{

}

void RbCircleLine::DrawBox(ID2D1HwndRenderTarget* pRenderTarget /*= nullptr*/)
{
	const D2D1_RECT_F& myBox = GetBox();
	pRenderTarget->DrawRectangle(myBox, HroadBrushManager::GetInstance().GetWhiteBrush(), 0.5);
}

D2D1_RECT_F RbCircleLine::GetBox()
{
	return m_Box;
}

void RbCircleLine::SetParam(HPoint2d start, HPoint2d end, D2D1_SIZE_F size, bool IsClockWise, bool arcSize)
{
	m_StartP = start;
	m_EndP = end;
	m_Size = size;
	m_SweepDirection = IsClockWise ? D2D1_SWEEP_DIRECTION_CLOCKWISE : D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE;
	m_ArcSize = arcSize ? D2D1_ARC_SIZE_LARGE : D2D1_ARC_SIZE_SMALL;


	HRESULT hr = E_FAIL;
	if (m_PathGeometry != nullptr)
	{
		SafeRelease(&m_PathGeometry);
	}

	ID2D1Factory* pFactory = HroadD2D1Factory::GetInstance().GetFactory();
	if (pFactory == nullptr)
	{
		return;
	}

	hr = pFactory->CreatePathGeometry(&m_PathGeometry);
	if (SUCCEEDED(hr))
	{
		ID2D1GeometrySink* pSink = nullptr;
		hr = m_PathGeometry->Open(&pSink); // 获取Sink对象

		if (SUCCEEDED(hr))
		{
			pSink->BeginFigure(m_StartP, D2D1_FIGURE_BEGIN_FILLED);

			D2D1_ARC_SEGMENT seg =
			{
				m_EndP,
				m_Size,
				m_RotationAngle,
				m_SweepDirection,
				m_ArcSize
			};

			pSink->AddArc(seg);
			// 添加图形 

			pSink->EndFigure(D2D1_FIGURE_END_OPEN);
		}

		pSink->Close(); // 关闭Sink对象
		SafeRelease(&pSink);

		m_PathGeometry->GetBounds(D2D1::Matrix3x2F::Identity(), &m_Box);
	}
}

double RbCircleLine::GetLength()
{
	throw std::logic_error("The method or operation is not implemented.");
}

double RbCircleLine::GetParam(HPoint2d const& pt, enumOff off /*= enumOff::enumSet*/)
{
	throw std::logic_error("The method or operation is not implemented.");
}

void RbCircleLine::DrawShape(ID2D1HwndRenderTarget* pRenderTarget)
{
	pRenderTarget->DrawGeometry(m_PathGeometry, HroadBrushManager::GetInstance().GetRedBrush());
}

std::tuple<HPoint2d, double> RbCircleLine::GetNearestPoint(HPoint2d const& p)
{
	HPoint2d p1 = StartPt();
	HPoint2d p2 = EndPt();
	float x1 = p1.X();
	float y1 = p1.Y();
	float x2 = p2.X();
	float y2 = p2.Y();
	float x = p.X();
	float y = p.Y();


	float cross = (x2 - x1) * (x - x1) + (y2 - y1) * (y - y1);


	// 大于90度
	if (cross <= 0)
	{
		return std::make_tuple(p1, sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1)));
	}

	// 向量的模
	float d2 = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
	if (cross >= d2)
	{
		return std::make_tuple(p2, sqrt((x - x2) * (x - x2) + (y - y2) * (y - y2)));
	}


	float r = cross / d2;
	float px = x1 + (x2 - x1) * r;
	float py = y1 + (y2 - y1) * r;
	return  std::make_tuple(HPoint2d(px, py), sqrt((x - px) * (x - px) + (y - py) * (y - py)));
}

CHroadCurve::CHroadCurve(HPoint2d p1, HPoint2d p2)
	: m_StartP(p1)
	, m_EndP(p2)
{

}

CHroadCurve::CHroadCurve()
{

}

CHroadCurve::~CHroadCurve()
{

}

std::tuple<HPoint2d, double> CHroadCurve::GetNearestPoint(HPoint2d const& pt)
{
	return std::tuple<HPoint2d, double>();
}
