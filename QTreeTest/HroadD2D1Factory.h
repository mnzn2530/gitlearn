#pragma once
#include <d2d1.h>


class HroadD2D1Factory
{
public:
	HroadD2D1Factory();
	~HroadD2D1Factory();

	ID2D1Factory* GetFactory();
	static HroadD2D1Factory& GetInstance();

private:
	static ID2D1Factory* m_pD2DFactory;
};