#include "HroadD2D1Factory.h"
#include "Direct2DHelloWorld.h"
#include "utils.h"


ID2D1Factory* HroadD2D1Factory::m_pD2DFactory = nullptr;


HroadD2D1Factory::HroadD2D1Factory()
{

}

HroadD2D1Factory::~HroadD2D1Factory()
{
	SafeRelease(&m_pD2DFactory);
}

ID2D1Factory* HroadD2D1Factory::GetFactory()
{
	HRESULT hr = E_FAIL;

	if (m_pD2DFactory == nullptr)
	{
		// Create a Direct2D factory.
		hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2DFactory);
	}

	return m_pD2DFactory;
}

HroadD2D1Factory& HroadD2D1Factory::GetInstance()
{
	static HroadD2D1Factory instance;
	return instance;
}

