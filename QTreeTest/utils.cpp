#include "utils.h"
#include "RbPolyLine.h"
#include <d2d1helper.h>



bool isPointOnSegment(HPoint2d const& p, RbLineSeg const& seg)
{
	HPoint2d p1 = seg.StartPt();
	HPoint2d p2 = seg.EndPt();


	//X积是否为0，判断是否在同一直线上
	if ((p1.X() - p.X()) * (p2.Y() - p.Y()) - (p2.X() - p.X()) * (p1.Y() - p.Y()) != 0)
	{
		return false;
	}

	//判断是否在线段上
	if ((p.X() > p1.X() && p.X() > p2.X()) || (p.X() < p1.X() && p.X() < p2.X()))
	{
		return false;
	}
	if ((p.Y() > p1.Y() && p.Y() > p2.Y()) || (p.Y() < p1.Y() && p.Y() < p2.Y()))
	{
		return false;
	}

	return true;
}

double Degree2Radius(double s)
{
	double r = s * PI / 180;
	return r;
}

double Radius2Degree(double s)
{
	double d = s * 180 / PI;
	return d;
}

HPoint2d PointExtendDis(HPoint2d pt, HPoint2d vec, double dis)
{
	HPoint2d UVec = vec.UnitVector();
	HPoint2d MoveVec = UVec * dis;
	HPoint2d DstPt = pt + MoveVec;
	return DstPt;
}

HPoint2d RotateByPoint(HPoint2d CenterPt, HPoint2d SrcPt, double nDgree)
{
	double dRadius = Degree2Radius(nDgree);
	D2D1::Matrix3x2F DstMatrix = D2D1::Matrix3x2F::Rotation(nDgree, SrcPt);
	return HPoint2d(DstMatrix.m[2][0], DstMatrix.m[2][1]);
}

HPoint2d LineSetVectorTop(RbLineSeg Line)
{
	HPoint2d DstPt;


	return DstPt;
}

double DotProduct(HPoint2d vec1, HPoint2d vec2)
{
	return vec1.X() * vec2.X() + vec1.Y() * vec2.Y();
}

HPoint2d CrossProduct(HPoint2d vec1, HPoint2d vec2)
{
	HPoint2d DstPt;


	return DstPt;
}
