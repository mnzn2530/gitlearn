#pragma once
#include <d2d1.h>
#include <vector>



class HroadBrushManager
{
public:
	HroadBrushManager();
	~HroadBrushManager();

	void Init(ID2D1HwndRenderTarget* pRenderTarget);
	static HroadBrushManager& GetInstance();

	ID2D1SolidColorBrush* GetWhiteBrush();
	ID2D1SolidColorBrush* GetGreenBrush();
	ID2D1SolidColorBrush* GetRedBrush();

	ID2D1SolidColorBrush* GetBrush(int index);


private:
	std::vector<ID2D1SolidColorBrush*> m_pBrushes;
};